{{include './partial/header.tpl' data={title:'uemo首页',bodyClass:'bodyindex'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
    <div id="scrollView" class="scrollView">
        <div class="indexPage">
            <!-- mslider -->
            <div id="mslider">
                <canvas id="renderSurface"></canvas>
                <div class="item_block">
                    <div class="item_wrapper">
                        <p class="title"><span>UEmo</span>极速建站</p>
                        <p class="subtitle">
                            拥有一个属于您的高品质网站，从现在开始
                        </p>
                    </div>
                    <div class="item_btn">
                        <div class="btn_left">
                            <a href="#">
                                免费试用
                            </a>
                        </div>
                        <div class="btn_right active">
                            <a href="#">
                                免费试用
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mcounter -->
            <div id="mcounter" class="mcounter module hide">
                <div class="module_container">
                    <div class="content_wrapper">
                        <div class="content_list clearfix">
                            <div class="item_block">
                                <p class="number" data-counter-value="24682">
                                    <span
                                        class="unit iconfont icon-jiahao1"
                                    ></span>
                                </p>
                                <p class="title">建站数量</p>
                            </div>
                            <div class="item_block">
                                <p class="number" data-counter-value="114">
                                    <span class="unit">套</span>
                                </p>
                                <p class="title">建站产品</p>
                            </div>
                            <div class="item_block">
                                <p class="number" data-counter-value="14">
                                    <span class="unit">年</span>
                                </p>
                                <p class="title">WEB服务</p>
                            </div>
                            <div class="item_block">
                                <p class="number" data-counter-value="376">
                                    <span
                                        class="unit iconfont icon-jiahao1"
                                    ></span>
                                </p>
                                <p class="title">覆盖城市</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- template -->
            <div id="template" class="template module">
                <div class="module_container">
                    <div class="container_header wow">
                        <p class="title">UEmo网站模板</p>
                        <p class="subtitle">
                            10年建站经验+800家企业客户+知名建站团队=UEmo高端企业建站
                        </p>
                    </div>
                    <div class="container_category wow">
                        <div class="header_btn">
                            <a href="javascript:;" class="item_btn choice">
                                <span>选择行业</span>
                                <i class="iconfont icon-fenlei1"></i>
                            </a>
                            <a href="javascript:;" class="item_btn random">
                                <span>随机欣赏</span>
                                <i class="iconfont icon-fenlei1"></i>
                            </a>
                        </div>
                        <div
                            class="category_wrapper"
                            data-menu-title="选择行业"
                        >
                            <a href="#" data-num="05">艺术设计</a>
                            <a href="#" data-num="05">摄影摄像</a>
                            <a href="#" class="active" data-num="05"
                                >建筑园林</a
                            >
                            <a href="#" data-num="05">企业集团</a>
                            <a href="#" data-num="05">家居百货</a>
                            <a href="#" data-num="05">医疗保健</a>
                            <a href="#" data-num="05">餐饮茶酒</a>
                            <a href="#" data-num="05">农工林牧</a>
                            <a href="#" data-num="05">教育培训</a>
                        </div>
                    </div>
                    <div class="container_content">
                        <div class="content_wrapper">
                            <div class="content_list">
                                <div class="item_block wow">
                                    <a href="./template_post.html">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/template1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper">
                                            <p class="title">蔬菜水果网站</p>
                                            <p class="subtitle">
                                                <span class="num">编号</span>
                                                <span class="id"
                                                    >mo005_21507</span
                                                >
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block wow">
                                    <a href="./template_post.html">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/template1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper">
                                            <p class="title">蔬菜水果网站</p>
                                            <p class="subtitle">
                                                <span class="num">编号</span>
                                                <span class="id"
                                                    >mo005_21507</span
                                                >
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block wow">
                                    <a href="./template_post.html">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/template1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper">
                                            <p class="title">蔬菜水果网站</p>
                                            <p class="subtitle">
                                                <span class="num">编号</span>
                                                <span class="id"
                                                    >mo005_21507</span
                                                >
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block wow">
                                    <a href="./template_post.html">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/template1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper">
                                            <p class="title">蔬菜水果网站</p>
                                            <p class="subtitle">
                                                <span class="num">编号</span>
                                                <span class="id"
                                                    >mo005_21507</span
                                                >
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block wow">
                                    <a href="./template_post.html">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/template1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper">
                                            <p class="title">蔬菜水果网站</p>
                                            <p class="subtitle">
                                                <span class="num">编号</span>
                                                <span class="id"
                                                    >mo005_21507</span
                                                >
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <a href="./template.html" class="more wow">
                                <span>查看全部</span>
                                <i
                                    class="iconfont icon-jiantou5-copy-copy-copy"
                                ></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- case -->
            <div id="case" class="case module" data-files="https://static.uemo.net">
                <div class="module_container">
                    <div class="container_header wow">
                        <p class="title">找找同行的案例</p>
                        <p class="subtitle">
                            看看他们的网站，这套模板这么多行业也都可以用哦！
                        </p>
                    </div>
                    <div class="container_category wow">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                    href="javascript:;"
                                    data-cats="art"
                                    class="swiper-slide active"
                                    >生活家居</a
                                >
                                <a href="javascript:;" data-cats="cream" class="swiper-slide"
                                    >艺术设计</a
                                >
                                <a href="javascript:;" data-cats="building" class="swiper-slide"
                                    >摄影摄像</a
                                >
                                <a href="javascript:;" data-cats="company" class="swiper-slide"
                                    >建筑园林</a
                                >
                                <a href="javascript:;" data-cats="smju" class="swiper-slide"
                                    >生活家居</a
                                >
                                <a href="javascript:;" data-cats="travel" class="swiper-slide"
                                    >艺术设计</a
                                >
                                <a href="javascript:;" data-cats="medical" class="swiper-slide">医疗保健</a>
                                <a href="javascript:;" data-cats="clothes" class="swiper-slide">服饰饰品</a>
                                <a href="javascript:;" data-cats="drink" class="swiper-slide">餐饮茶酒</a>
                                <a href="javascript:;" data-cats="town" class="swiper-slide">农工林牧</a>
                                <a href="javascript:;" data-cats="education" class="swiper-slide">教育培训</a>
                                <a href="javascript:;" data-cats="smart" class="swiper-slide">智能设备</a>
                            </div>
                        </div>
                    </div>
                    <div class="container_content wow">
                        <div class="wrapper-slider swiper-container">
                            <div class="swiper-wrapper">
                                <div
                                    class="item_block swiper-slide"
                                    data-case-tag="0"
                                >
                                    <div class="head">
                                        <i class="iconfont icon-gengduo"></i>
                                    </div>
                                    <div class="item_img">
                                        <img
                                            src="./assets/images/template1.jpg"
                                            alt=""
                                        />
                                    </div>
                                    <div class="item_wrapper">
                                        <p class="title">
                                            福建十一启健康管理有限公司
                                        </p>
                                    </div>
                                </div>
                                <div
                                    class="item_block swiper-slide"
                                    data-case-tag="0"
                                >
                                    <div class="head">
                                        <i class="iconfont icon-gengduo"></i>
                                    </div>
                                    <div class="item_img">
                                        <img
                                            src="./assets/images/template1.jpg"
                                            alt=""
                                        />
                                    </div>
                                    <div class="item_wrapper">
                                        <p class="title">
                                            福建十一启健康管理有限公司
                                        </p>
                                    </div>
                                </div>
                                <div
                                    class="item_block swiper-slide"
                                    data-case-tag="0"
                                >
                                    <div class="head">
                                        <i class="iconfont icon-gengduo"></i>
                                    </div>
                                    <div class="item_img">
                                        <img
                                            src="./assets/images/template1.jpg"
                                            alt=""
                                        />
                                    </div>
                                    <div class="item_wrapper">
                                        <p class="title">
                                            福建十一启健康管理有限公司
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- team -->
            <div id="team" class="team module">
                <div class="module_container">
                    <div class="container_header wow">
                        <p class="title">我们的客户都怎么说</p>
                        <p class="subtitle">
                            看看他们的网站，这套模板这么多行业也都可以用哦！
                        </p>
                    </div>
                    <div class="container_content">
                        <div class="content_des">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="item_block wow swiper-slide">
                                        <div class="des">
                                            创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。感谢UEMO与黑金经纪一路相伴，以现代的设计理念与完善的技术服务支持，让我们看到互联网世界中的最美黑金！
                                        </div>
                                    </div>
                                    <div class="item_block wow swiper-slide">
                                        <div class="des">
                                            创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现
                                        </div>
                                    </div>
                                    <div class="item_block wow swiper-slide">
                                        <div class="des">
                                            创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。感谢UEMO与黑金经纪一路相伴，以现代的设计理念与完善的技术服务支持，让我们看到互联网世界中的最美黑金！
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content_info">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="item_block wow swiper-slide">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/team_img.png"
                                                alt="img"
                                            />
                                        </div>
                                        <div class="item_info">
                                            <p class="title ellipsis">聂心远</p>
                                            <p class="subtitle ellipsis">
                                                黑金经纪创始人兼董事长
                                            </p>
                                        </div>
                                        <div class="item_autograph">
                                            <img
                                                src="./assets/images/team_autograph.jpg"
                                                alt="autograph"
                                            />
                                        </div>
                                    </div>
                                    <div class="item_block wow swiper-slide">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/team_img.png"
                                                alt="img"
                                            />
                                        </div>
                                        <div class="item_info">
                                            <p class="title ellipsis">聂心远</p>
                                            <p class="subtitle ellipsis">
                                                黑金经纪创始人兼董事长
                                            </p>
                                        </div>
                                        <div class="item_autograph">
                                            <img
                                                src="./assets/images/team_autograph.jpg"
                                                alt="autograph"
                                            />
                                        </div>
                                    </div>
                                    <div class="item_block wow swiper-slide">
                                        <div class="item_img">
                                            <img
                                                src="./assets/images/team_img.png"
                                                alt="img"
                                            />
                                        </div>
                                        <div class="item_info">
                                            <p class="title ellipsis">聂心远</p>
                                            <p class="subtitle ellipsis">
                                                黑金经纪创始人兼董事长
                                            </p>
                                        </div>
                                        <div class="item_autograph">
                                            <img
                                                src="./assets/images/team_autograph.jpg"
                                                alt="autograph"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- news -->
            <div id="news" class="news module">
                <div class="module_container wow">
                    <div class="container_header">
                        <p class="title">帮助中心</p>
                    </div>
                    <div class="container_category wow">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <a href="javascript:;" class="swiper-slide"
                                    >选择模板</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >SEO</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >域名问题</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >特色功能</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >管理网站</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >新闻咨询</a
                                >
                                <a href="javascript:;" class="swiper-slide"
                                    >使用教程</a
                                >
                                <a href="#" class="swiper-slide more">更多</a>
                            </div>
                        </div>
                    </div>
                    <div class="container_content">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="content_list swiper-slide">
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item_block wow">
                                        <a href="./news.html">
                                            <p class="title ellipsis">
                                                个人、企业网站建设常见问题 Q&A
                                            </p>
                                            <div class="des">
                                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mcustomize -->
            <div id="mcustomize" class="mcustomize">
                <div class="module_container">
                    <div class="container_content">
                        <div class="header wow">
                            <div class="title">开启您的建站之旅吧</div>
                        </div>
                        <div class="description wow">
                            我们深知创业艰难，守业更难，一个容易让意向客户一秒铭记品牌的官网，助您企业上升，一个视觉美观的官网，能够在竞价推广时在众多网站中脱颖而出，一个便于优化和更新的官网，能为您省下资金做更有用的事；一次试用，能让未来的您，感激现在自己的决定
                        </div>
                        <a href="#" class="free_btn wow">免费试用</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mask"></div>
</div>
{{include './partial/footer.tpl' data={active:'index'} }}
