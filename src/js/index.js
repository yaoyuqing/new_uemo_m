// main
import "../css/main.scss";
import "./child.js";

if (module.hot) {
  module.hot.accept("./child.js", function () {
    console.log("Accepting the updated printMe module!");
  });
  // 关闭指定子模块的HMR
  // module.hot.decline('./extra.js')
}
