// wow.js
import { WOW } from "wowjs";

// swiper.js
import Swiper from "swiper";
// @cycjimmy/swiper-animation
// import SwiperAnimation from '@cycjimmy/swiper-animation'

// appear.js
// import 'jquery-appear-original'

// malihu-custom-scrollbar-plugin
import "malihu-custom-scrollbar-plugin";

// jquery-mousewheel
import "jquery-mousewheel";

// Hammer.js
// var Hammer = require('../../node_modules/Hammerjs/hammer')

// // qrcode.js
// import QRCoder from 'qrcoder'

// webgl-fluid
import WebglFluid from "webgl-fluid/dist/webgl-fluid.umd.js";

// lulu ui
import LightTip from "pure/js/common/ui/LightTip";

$(function () {
  header();
  searchBox();
  pageIndex();
  pageTemplate();
  pageCustomer();
  pageDiy();
  pageTemplatepost();
  pageDemo();
  pageNews();
  pageNewspost();
  templateStorage();
  initCategory();
  // initQrcode()
  // initWow()
  imgPreviewDom($(".side-nav .share .weixin"));
  pageSearch();
});

window.$ = $;

// 禁止放大
window.onload = () => {
  document.addEventListener(
    "touchstart",
    (event) => {
      if (event.touches.length > 1) {
        event.preventDefault();
      }
    },
    { passive: false }
  );

  document.addEventListener(
    "gesturestart",
    (event) => {
      if (event.touches.length > 1) {
        event.preventDefault();
      }
    },
    { passive: false }
  );

  let lastTouchEnd = 0;
  document.addEventListener(
    "touchend",
    (event) => {
      const now = new Date().getTime();
      if (now - lastTouchEnd <= 300) {
        event.preventDefault();
      }
      lastTouchEnd = now;
    },
    false
  );
};

// validator
var pattern = {
  phone: /^0?(13|14|15|17|18|19)[0-9]{9}$/,
  isEmpty: /.+/,
  password: /.{6}/,
  code: /^\d{6}$/,
  email: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
};

var loadingDom =
  '<div class="pay_loading_dom"><div class="pay_loading_dom_wrapper"><div class="pay_message"><div href="javascript:;" style="padding: 10px 0;" class="pay_logo"><i class="iconfont icon-uemologo-copy" style="font-size: 32px;color: #333;"></i></div><span class="text">请稍后...</span></div><div class="windows8"> <div class="wBall" id="wBall_1"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_2"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_3"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_4"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_5"> <div class="wInnerBall"></div> </div> </div></div></div>';


// ajax响应拦截
$.ajaxSetup({
  contentType: "application/x-www-form-urlencoded;charset=utf-8",
  complete: function (XMLHttpRequest, textStatus) {
    let result;

    if (typeof XMLHttpRequest.responseJSON === "string") {
      return XMLHttpRequest.responseJSON;
    } else if (typeof XMLHttpRequest.responseJSON !== "object") {
      new LightTip().error("<span>接口错误</span>", 2000);
      return Promise.reject(new Error("接口错误")).catch(() => {});
    } else if (XMLHttpRequest.responseJSON.code) {
      switch (String(XMLHttpRequest.responseJSON.code)) {
        case "0":
          result = Promise.resolve(XMLHttpRequest.responseJSON.data);
          break;
        case "1":
          var msg = XMLHttpRequest.responseJSON.msg;
          new LightTip().error("<span>" + msg + "</span>", 2000);
          break;
        case "997":
          $('body').addClass('openuser');
          break;
        case "998":
        case "999":
          window.location = XMLHttpRequest.responseJSON.msg;
          break;
        default:
          result = Promise.reject(XMLHttpRequest);
          break;
      }
      return result;
    }
  },
});

// header
function header() {
  if ($(document).scrollTop() > 0) {
    $("body").addClass("mini");
  }
  $(document).scroll(function () {
    if ($(document).scrollTop() > 0) {
      $("body").addClass("mini");
    } else {
      $("body").removeClass("mini");
    }
  });

  // 导航展开
  var navLen = $(".side-nav .nav_wrapper .nav_item").length;
  $(".side-nav .nav_wrapper .nav_item").each(function (i, el) {
    $(this).css({
      "animation-delay": i * 0.075 + "s",
    });
  });
  $(".side-nav .contact .wow").each(function (i, el) {
    $(this).css({
      "animation-delay": (i + navLen) * 0.075 + "s",
    });
  });

  $(".mm-hbtn").on("click", function () {
    $("body").addClass("show");
    $(".sitecontent").addClass("hidden");
    forceWowReanimation(".side-nav .wow");
  });
  $(".mask").on("click", function () {
    $("body").removeClass("show");
    TransitionEnd($(".sitecontent"), function () {
      $(".sitecontent").removeClass("hidden");
    });
  });

  var pathname = window.location.pathname;
  $(".side-nav .nav_wrapper .nav_item").each(function (i, el) {
    var href = $(this).attr("href");
    if (href === pathname) {
      $(el).addClass("active").siblings().removeClass("active");
    }
  });
}

// 搜索
function searchBox() {
  var $searchBox = $(".search-box");
  var $input = $(".search_bar .searchform_input", $searchBox);
  var $submit = $(".search_bar .searchform_submit", $searchBox);
  var searchClass = new SearchBox({ elm: $searchBox });

  $("#header .search").click(function () {
    searchClass.show();
  });

  $searchBox.on("click", function () {
    if (event.target === this) {
      searchClass.hide();
      $input.val("");
    }
  });

  $(".search_bar .search_clear", $searchBox).click(function () {
    searchClass.hide();
  });

  // 点击搜索
  $submit.on("click", function () {
    var searchAddr = window.location.origin;
    var inpVal = $input.val();
    window.location.href = `${searchAddr}/search/${inpVal}`;
  });

  $input.on("keydown", function (ev) {
    if (ev.keyCode === 13) {
      var searchAddr = window.location.origin;
      var inpVal = $(this).val();
      window.location.href = `${searchAddr}/search/${inpVal}`;
    }
  });
}

// 自定义菜单
function Category($category, $btn) {
  var title = $category.data("menu-title");
  var $overlay = $('<div class="category_overlay"></div>');
  var $menu = $(`<div class="category_menu">
      <div class="head">${title}</div>
      <div class="swiper-container">
          <ul class="content_list swiper-wrapper">
          </ul>
          <div class="swiper-scrollbar"></div>
      </div>
      <div class="foot">取消</div>
  </div>`);
  // var $menu = $('.category_menu')
  var $menuCont = $(".content_list", $menu);
  var str = "";
  !$("a", $category).filter(".active")[0] &&
    $("a", $category).eq(0).addClass("active");

  $("a", $category).each(function (i, a) {
    str += `<li class="item_cate swiper-slide ${
      $(a).hasClass("active") ? "active" : ""
    }">
    <a href="${$(a).attr("href")}">
        <span class="text">${$(a).text()}</span>
        ${
          $(a).data("num") === undefined
            ? ""
            : '<span class="count">' + $(a).data("num") + "</span>"
        }
    </a>
    </li>`;
  });
  $menuCont.html(str);

  $("body").append($overlay, $menu);
  var $cateSlider = $(".swiper-container", $menu);
  var $curentEl = $(".swiper-slide.active", $cateSlider);
  var curentIndex = $curentEl.index() >= 0 ? $curentEl.index() : 0;

  initSlider($cateSlider, {
    direction: "vertical",
    slidesPerView: 7,
    initialSlide: curentIndex,
    scrollbar: {
      el: ".swiper-scrollbar",
      draggable: true,
    },
  });

  $btn.click(function () {
    $overlay.addClass("show");
    $menu.addClass("show");
  });
  var $close = $(".foot", $menu);
  $close.click(function () {
    $overlay.removeClass("show");
    $menu.removeClass("show");
  });
  $overlay.on("click", function () {
    if (event.target === this) {
      $overlay.removeClass("show");
      $menu.removeClass("show");
    }
  });
}

// 首页
function pageIndex() {
  $(".bodyindex").each(function () {
    indexSlider();
    indexCounter();
    indexCase();
    indexTeam();
    indexNews();
    indexTemplate();
  });
}

// 首页幻灯
function indexSlider() {
  var $slider = $("#mslider .swiper-container");
  initSlider($slider, {
    loop: true,
    autoplay: {
      disableOnInteraction: false,
      delay: 3000,
    },
  });

  WebglFluid(document.querySelector("#renderSurface"), {
    IMMEDIATE: false,
    SIM_RESOLUTION: 256,
    DYE_RESOLUTION: 1024,
    CAPTURE_RESOLUTION: 512,
    DENSITY_DISSIPATION: 3,
    VELOCITY_DISSIPATION: 0,
    PRESSURE: 1,
    PRESSURE_ITERATIONS: 20,
    CURL: 0,
    SPLAT_RADIUS: 0.25,
    SPLAT_FORCE: 6000,
    SHADING: true,
    COLORFUL: true,
    COLOR_UPDATE_SPEED: 10,
    PAUSED: false,
    BACK_COLOR: { r: 89, g: 66, b: 210 },
    TRANSPARENT: false,
    BLOOM: true,
    BLOOM_ITERATIONS: 8,
    BLOOM_RESOLUTION: 256,
    BLOOM_INTENSITY: 0.8,
    BLOOM_THRESHOLD: 0.6,
    BLOOM_SOFT_KNEE: 0.7,
    SUNRAYS: true,
    SUNRAYS_RESOLUTION: 196,
    SUNRAYS_WEIGHT: 1.0,
  });
}

// 首页数字滚动
function indexCounter() {
  $("#mcounter .content_list .item_block .number").each(function (i, el) {
    var number = $(el).data("counter-value");
    var numArr = ("" + number).split("");
    var str = "";
    for (let i = 0; i < numArr.length; i++) {
      str += `<span class="counterDX wow">${numArr[i]}</span>`;
    }
    $(".unit", el).before(str);
    $(".counterDX", el).each(function (j, n) {
      $(n).attr("data-wow-delay", `${(j * 0.12).toFixed(1)}s`);
    });
  });
}

// 首页模板
function indexTemplate() {
  $("#template").each(function (i, el) {
    var scrollTo = sessionStorage.getItem("scrollTop") || 0;
    $(".scrollView").scrollTop(scrollTo);
    Category(
      $(".container_category .category_wrapper", el),
      $(".container_category .choice", el)
    );
  });
}

// 首页案例
function indexCase() {
  $("#case").each(function (i, el) {
    var $sliderCategory = $(".container_category .swiper-container", el);
    var swiperCategory = initSlider($sliderCategory, {
      slidesPerView: "auto",
      slideToClickedSlide: true,
      watchSlidesVisibility: true,
      spaceBetween: "2.5%",
    });

    var $slider = $(".container_content .wrapper-slider", el);
    var swiperSlider = initSlider($($slider), {
      slidesPerView: "auto",
      spaceBetween: "6%",
      roundLengths: false,
      centeredSlides: true,
      centeredSlidesBounds: true,
      observer: true,
      on: {
        slideChange: function () {
          var gotoIndex = $(this.slides[this.activeIndex]).data("case-tag");
          swiperCategory.slideTo(gotoIndex);
          $(".container_category a", el).removeClass("active");
          $(".container_category a", el).eq(gotoIndex).addClass("active");
        },
      },
    });

    var result = [];
    $(".container_category a", el).each(function (n, a) {
      if(n > 0) {
        result.push($(a).data("cats"));
      }
      $(a).on("click", function () {
        $(".container_category a", el).removeClass("active");
        $(this).addClass("active");
        var gotoIndex = $(
          `.container_content .item_block[data-case-tag="${n}"]:first`,
          el
        ).index();
        swiperSlider.slideTo(gotoIndex);
      });
    });

    var fileUrl = $(el).data("files");
    getData(result, 1);
    function getData(idArr, i) {
      if (idArr.length) {
        var newparam = idArr[0];
        idArr.shift();
        $.ajax({
          url: "/shop/openapi/get_cat_info",
          data: {
            name: newparam,
          },
          success: function (res) {
            if (res.code === 0) {
              var data = res.data.sites;
              var str = "";
              $.each(data, function (index, item) {
                let _title = item.title ? item.title : "";
  
                str +=
                  '<div class="item_block swiper-slide" data-case-tag="' +
                  i +
                  '"><div class=head><i class="icon-gengduo iconfont"></i></div><div class=item_img><img alt=""src="' +
                  fileUrl +
                  item.img +
                  '"></div><div class=item_wrapper><p class=title>' +
                  _title +
                  "</div></div>";
              });
              $(`.wrapper-slider .swiper-wrapper`, el).append(str);
              getData(idArr, ++i);
            }
          },
          error: function () {
            getData(idArr, ++i);
          },
        });
      }
    }
  });
}

// 首页团队
function indexTeam() {
  $("#team").each(function (i, el) {
    var $sliderDes = $(".content_des .swiper-container", el);
    var swiperDes = initSlider($sliderDes, {
      loop: true,
      slidesPerView: "auto",
      loopedSlides: 1,
      centeredSlides: true,
      centeredSlidesBounds: true,
      spaceBetween: "6%",
    });

    var $sliderInfo = $(".content_info .swiper-container", el);
    var swiperInfo = initSlider($sliderInfo, {
      loop: true,
      slidesPerView: 1,
      loopedSlides: 1,
    });

    swiperDes.controller.control = swiperInfo;
    swiperInfo.controller.control = swiperDes;
  });
}

// 首页新闻
function indexNews() {
  $("#news").each(function (i, el) {
    var $sliderCategory = $(".container_category .swiper-container", el);
    var swiperCategory = initSlider($($sliderCategory), {
      slidesPerView: "auto",
      watchSlidesVisibility: true,
      spaceBetween: "2.5%",
    });

    var $sliderCon = $(".container_content .swiper-container", el);
    initSlider($sliderCon, {
      simulateTouch: false,
      allowTouchMove: false,
      autoHeight: true,
      thumbs: {
        swiper: swiperCategory,
        autoScrollOffset: 2,
      },
    });
  });
}

// 模板列表页
function pageTemplate() {
  $(".bodytemplate").each(function (i, el) {
    var scrollTo = sessionStorage.getItem("scrollTop") || 0;
    $(".scrollView").scrollTop(scrollTo);
  });
}

// 案列列表页
function pageCustomer() {
  $(".bodycustomer").each(function (i, el) {});
}

// 定制列表页
function pageDiy() {
  $(".bodydiy").each(function (i, el) {});
}

// 列表页分类
function initCategory() {
  $(".bodytemplate, .bodycustomer, .bodydiy, .bodynews").each(function (i, el) {
    var currentCategory =
      $(".page_target .category_wrapper a.active", el).text() ||
      $(".page_target .category_wrapper a", el).eq(0).text();
    $(".page_target .current_category span", el).text(currentCategory);
    Category(
      $(".page_target .container_category .category_wrapper", el),
      $(".page_target .container_category .current_category", el)
    );
  });
}

// 模板详情页
function pageTemplatepost() {
  $(".bodytemplatepost").each(function (i, el) {
    // 头部返回
    $(".back_btn", el).click(function () {
      window.history.back();
    });

    var $userSlider = $(".user .content_wrapper", el);
    initSlider($userSlider, {
      slidesPerView: "auto",
      spaceBetween: "6%",
    });
    // 分类
    var $cateSlider = $(".listContent .container_category", el);
    var curentIndex = $(".swiper-slide.active", $cateSlider).index();
    var swiperCategory = initSlider($cateSlider, {
      slidesPerView: "auto",
      initialSlide: curentIndex,
      spaceBetween: "2.5%",
      slideToClickedSlide: true,
      watchSlidesVisibility: true,
    });

    var $contentSlider = $(".listContent .container_content .swiper-container");
    initSlider($contentSlider, {
      autoHeight: true,
      spaceBetween: "6%",
      simulateTouch: false,
      allowTouchMove: false,
      thumbs: {
        swiper: swiperCategory,
        autoScrollOffset: 1,
      },
    });

    // footer btn
    var $postBtn = $(".post_btn_wrapper", el).clone(true);
    $("#footer").html($postBtn);

    $(document).scroll(function () {
      if ($(document).scrollTop() > $(".banner", el).outerHeight()) {
        $("#footer").addClass("show");
      } else {
        $("#footer").removeClass("show");
      }
    });

    // 判断是否登录
    $(".post_btn.try", el).on("click", function (e) {
      e.preventDefault();
      $(loadingDom).appendTo("body");

      var siteId = $(this).data("id");
      $.ajax({
        type: "GET",
        async: true,
        url: "/shop/api/to_try",
        data: {
          site_id: siteId,
        },
        dataType: "json",
        beforeSend: function () {
          $(loadingDom).appendTo("body");
        },
        success: function () {
          $(".pay_loading_dom").remove();
        },
        error: function () {
          $(".pay_loading_dom").remove();
        },
      });
    });

    // 基本信息补全
    var getuserinfo = {};
    var userinfo = {
      company_name: "",
      email: "",
      qq: ""
    };
    var dataFormState = {
      company_name: -1,
      email: -1,
      qq: -1
    };

    initUserInfo(initInput);

    function initUserInfo(callback) {
      $.get("/shop/api/userinfo", function (data) {
        getuserinfo = data.data;
        userinfo = {
          company_name: getuserinfo.ainfo.name,
          contacts: getuserinfo.ainfo.contacter,
          email: getuserinfo.email,
          qq: getuserinfo.ainfo.qq
        };
        callback && callback();
      });
    }

    function initInput() {
      $(".user-setting .custom_el_input").each(function (i, elm) {
        var inputName = $("input", elm).attr("name");
        var isRequired = $(elm).hasClass("custom_el_input_required");
        var _pattern = pattern[inputName];
        var errMsgEl =
          $(".err_msg", elm).text().length > 2 && $(".err_msg", elm);
        $("input", elm).val(userinfo[inputName]);
        validator($("input", elm));

        $(elm).off('focusin').on("focusin", function () {
          $(this).addClass("state_focus");
        });
        $(elm).off('focusout').on("focusout", function () {
          $(this).removeClass("state_focus");
        });
        $("input", elm).off('input').on("input", function () {
          var _val = $(this).val().trim();
          userinfo[inputName] = _val;
          var notEmpty = pattern.isEmpty.test(_val);
          if (isRequired && !notEmpty) {
            $(this)
              .closest(".custom_el_input")
              .removeClass("not_empty")
              .addClass("state_error");
            errMsgEl && errMsgEl.css({opacity: 1});
          } else {
            $(this)
              .closest(".custom_el_input")
              .addClass("not_empty")
              .removeClass("state_error");
            errMsgEl && errMsgEl.css({opacity: 0});
          }
          validator($(this));
        });
        $("input", elm).off('blur').on("blur", function () {
          validator($(this));
        });

        function validator($input) {
          var _val = $input.val().trim();
          var $customInp = $input.closest(".custom_el_input");
          var notEmpty = pattern.isEmpty.test(_val);
          var isSuc =
            notEmpty && _pattern
              ? _pattern.test(_val)
              : pattern.isEmpty.test(_val);

          if (notEmpty) {
            $customInp.addClass("not_empty");
          } else {
            $customInp.removeClass("not_empty");
          }
          if (isRequired) {
            if (notEmpty && isSuc) {
              $customInp.removeClass("state_error");
              errMsgEl && errMsgEl.css({opacity: 0});
            } else {
              $customInp.addClass("state_error");
              errMsgEl && errMsgEl.css({opacity: 1});
            }
          } else {
            if (notEmpty) {
              if (isSuc) {
                $customInp.removeClass("state_error");
                errMsgEl && errMsgEl.css({opacity: 0});
              } else {
                $customInp.addClass("state_error");
                errMsgEl && errMsgEl.css({opacity: 1});
              }
            } else {
              $customInp.removeClass("state_error");
              errMsgEl && errMsgEl.css({opacity: 0});
            }
          }

          dataFormState[$input.attr("name")] = !$customInp.hasClass(
            "state_error"
          )
            ? 1
            : -1;
          getUserFormIsChange();
        }

        function getUserFormIsChange() {
          let originValue = {
            company_name: getuserinfo.ainfo.name,
            contacts: getuserinfo.ainfo.contacter,
            email: getuserinfo.email,
            qq: getuserinfo.ainfo.qq
          };
          let isChange = Object.entries(userinfo).some(([key, value]) => {
            return value !== originValue[key];
          });

          let isValidate = Object.values(dataFormState).every(
            (item) => item > 0
          );

          $(".user-setting .submit_btn", el).toggleClass(
            "state-disable",
            !(isValidate && isChange)
          );
        }
      });

      $(".user-setting .submit_btn", el).off().on("click", function () {
        if($(this).hasClass('state-disable')) return false;
        $.post(
          "/shop/api/user_update",
          {
            name: userinfo.company_name,
            qq: userinfo.qq,
            contacter: userinfo.contacts,
            email: userinfo.email
          },
          function () {
            initUserInfo(initInput);
            $('body').removeClass('openuser');
            $(".post_btn.try", el).trigger("click");
          }
        );
      });
    }
  });
}

// demo页面
function pageDemo() {
  $(".demo").each(function (i, el) {
    var $back = $(".demo_btn_wrapper .back", el);
    var $iFrame = $("#contentFrame");
    var iframe = document.querySelector("#contentFrame");
    var $mask = $("#mask", el);

    $back.click(function () {
      window.history.back();
    });

    $iFrame.attr("src", $iFrame.data("src")).on("load", function () {
      $mask.fadeOut("slow");

      var iframeBody =
        iframe.contentWindow.document.querySelector("#sitecontent");
      var topValue = 0;
      var interval = null;
      $(iframeBody).scroll(function () {
        $("#footer", el).removeClass("show");
        if (interval === null) {
          interval = setInterval(function () {
            test();
          }, 1000);
        }
        topValue = $(iframeBody).scrollTop();
      });

      function test() {
        if ($(iframeBody).scrollTop() === topValue) {
          clearInterval(interval);
          interval = null;
          $("#footer", el).addClass("show");
        }
      }
    });

    var $menu = $(".category_menu");
    $("#footer .try", el).click(function () {
      $(".category_menu").addClass("show");
    });

    var $close = $(".foot", $menu);
    $close.click(function () {
      $menu.removeClass("show");
    });
    $menu.on("click", function () {
      if (event.target === this) {
        $menu.removeClass("show");
      }
    });
  });
}

// 新闻列表页
function pageNews() {
  $(".bodynews").each(function (i, el) {});
}

// 新闻详情页
function pageNewspost() {
  $(".bodynewspost").each(function (i, el) {
    // 相关分类滚动
    var $cateSlider = $(".listContent_post .item_tags", el);
    var curentIndex = $(".swiper-slide.active", $cateSlider).index();
    var swiperCategory = initSlider($cateSlider, {
      slidesPerView: "auto",
      initialSlide: curentIndex,
      spaceBetween: "2.5%",
      slideToClickedSlide: true,
      watchSlidesVisibility: true,
    });

    var $sliderCon = $(".container_content .swiper-container", el);
    initSlider($sliderCon, {
      simulateTouch: false,
      allowTouchMove: false,
      autoHeight: true,
      thumbs: {
        swiper: swiperCategory,
        autoScrollOffset: 2,
      },
    });
  });
}

// 搜索结果页
function pageSearch() {
  $('.bodysearch').each(function(i, el) {
    var $sliderCategory = $(".page_category .swiper-container", el);
    var $arrow = $('.arrow_right', $sliderCategory);
    var curentIndex = $(".swiper-slide.active", $sliderCategory).index();
    initSlider($sliderCategory, {
      slidesPerView: "auto",
      initialSlide: curentIndex,
      slideToClickedSlide: true,
      watchSlidesVisibility: true,
      on:{
        init: isEnd,
        sliderMove: isEnd
      },
    });

    function isEnd(){
      if(this.isEnd) {
        $arrow.fadeOut();
      } else {
        $arrow.fadeIn();
      }
    }
  });
}

// sessionStorage
function templateStorage() {
  $(".template").each(function (i, el) {
    $(".item_block", el).each(function (n, item) {
      $(item).click(function () {
        var scrollTopVal = $(".scrollView").scrollTop();
        sessionStorage.setItem("scrollTop", scrollTopVal);
      });
    });
  });
}

// wowinit
// function initWow () {
//   var wow = new WOW({
//     // scrollContainer: '#scrollView',
//     live: true,
//     offset: 50
//   })
//   wow.init()
// }

// 图片弹窗
function imgPreviewDom($elm) {
  var previewDom = $('<div class="img_preview_dom"></div>');

  $elm.on("click", function () {
    var url = $(this).data("src");
    previewDom.append('<img src="' + url + '" />');
    $("body").append(previewDom);
    previewDom.fadeIn();

    $(".img_preview_dom").on("click", function (ev) {
      if (ev.target === $(this)[0]) {
        previewDom.fadeOut(function () {
          $(this).remove();
        });
      }
    });
  });
}

// // qrcode
// function initQrcode () {
//   if (!$('.qrcode_wrapper')[0]) { return false }
//   var url = $('.qrcode_wrapper').data('src')
//   const qr = new QRCoder({
//     data: url,
//     size: 400
//   })
//   var dataURL = qr.getDataURL()
//   $('.qrcode').attr('src', dataURL)
// }

// 幻灯初始化
function initSlider($slider, options) {
  if (!$slider[0]) return;
  var settings = {
    roundLengths: true,
    watchOverflow: true,
    autoplay: false,
    loop: false,
    speed: 1000,
    direction: "horizontal",
    init: false,
  };

  var slickOpt = $.extend(settings, options);
  var myswiper = new Swiper($slider, slickOpt);

  myswiper.init();
  return myswiper;
}

// // 动画结束后回调
// function AnimationEnd (element, complete) {
//   var t
//   var el = document.createElement('fakeelement')

//   var animations = {
//     'animation': 'animationend',
//     'OAnimation': 'oAnimationEnd',
//     'MozAnimation': 'animationend',
//     'WebkitAnimation': 'webkitAnimationEnd'
//   }

//   for (t in animations) {
//     if (el.style[t] !== undefined) {
//       element.one(animations[t], function () {
//         element.off(animations[t])
//         if (complete) complete.call($(element)[0])
//       })
//     }
//   }
// }

// 点击空白关闭
// function blankClose ($elm, cb) {
//   $(document).click(function (e) {
//     var n = $elm
//     if (!n.is(e.target) && n.has(e.target).length === 0) {
//       cb()
//     }
//   })
// }

// 过渡结束后回调
function TransitionEnd(element, complete) {
  element.on("transitionend", function () {
    element.off("transitionend");
    if (complete) complete.call($(element)[0]);
  });
}

class SearchBox {
  /* eslint-disable */
  constructor(options) {
    this.$box = options.elm;
    this.$input = $("input", this.$box);
    this.$resultHistory = $(".history_word", this.$box);
    this.$clearBtn = $(".search_results .clear_btn", this.$box);
  }

  init() {}

  show() {
    this.$box.addClass("show");
    this.$input.focus();
  }

  hide() {
    this.$box.removeClass("show");
    this.$input.val("");
  }

  clearHistory() {
    $(".result_words", this.$resultHistory).html("");
    this.$clearBtn.on("click", function () {});
  }
}

// wow repeat
function forceWowReanimation(element) {
  var wow = new WOW({ scrollContainer: "body" });
  var wowList = document.querySelectorAll(element);
  wowList.forEach((n, i) => {
    n.classList.remove("animated");
    n.style.removeProperty("animation-name");
    wow.applyStyle(n, true);
    setTimeout(function () {
      wow.show(n);
    }, 10);
  });
}
