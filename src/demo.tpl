<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>demo</title>
        <link rel="stylesheet" href="css/reset.css" />
    </head>
    <body class="demo">
        <div id="sitecontent" class="sitecontent">
            <div id="scrollView" class="scrollView">
                <div id="contentArea" class="contentArea">
                    <iframe
                        id="contentFrame"
                        class="contentFrame"
                        src="/html/test.html"
                        data-src="/html/test.html"
                        frameborder="no"
                        border="0"
                        marginwidth="0"
                        marginheight="0"
                        allowtransparency="yes"
                    ></iframe>
                    <div id="mask" class="loading"></div>
                </div>
            </div>
        </div>
        <footer id="footer" class="footer show">
            <div class="demo_btn_wrapper">
                <a href="javascript:;" class="back">
                    <i class="iconfont icon-jiantou5-copy-copy"></i>
                    <span class="text">返回</span>
                </a>
                <a href="#" class="try_btn try">
                    免费试用此模板
                </a>
            </div>
        </footer>
        <div class="category_menu">
            <div class="category_wrapper">
                <div class="head">联系客服</div>
                <div class="wxcode">
                    <img
                        class="qrcode"
                        src="./assets/images/qrcode.png"
                        alt=""
                    />
                    <span class="text">长按联系微信客服</span>
                </div>
                <div class="item_cate active">
                    <a href="tel:1312188933">
                        <div class="name">
                            <span>联系人：</span>
                            <span class="name">杨经理</span>
                        </div>
                        <div class="tel">
                            <i class="iconfont icon-dianhua"></i>
                            <span class="num">1312188933</span>
                        </div>
                    </a>
                </div>
                <div class="foot">取消</div>
            </div>
        </div>
    </body>
</html>
