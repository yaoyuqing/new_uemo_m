<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>{{ title }}</title>
</head>

<body class="{{ bodyClass }}">
<link href="https://new.uemo.net/views/m/files/others.css" rel="stylesheet">
    <!-- 头部 -->
    <header id="header" class="">
        <a href="javascript:;" class="mm-hbtn">
            <div class="lcbody">
                <div class="lcitem top">
                    <div class="rect top"></div>
                </div>
                <div class="lcitem bottom">
                    <div class="rect bottom"></div>
                </div>
            </div>
        </a>
        <a class="mm-hlogo" href="./index.html">
            <i class="iconfont icon-uemologo-copy"></i>
        </a>
        <a class="search" href="javascript:;">
            <i class="iconfont icon-sousuo-copy"></i>
        </a>
    </header>
    <div id="search-box" class="search-box">
        <div class="search_bar">
            <div class="search_bar_wrapper">
                <form action="" id="searchform" class="searchform">
                    <input type="text" placeholder="" class="searchform_input" />
                    <button id="searchform_submit" class="searchform_submit" disabled="">
                        <i class="iconfont icon-sousuo-copy"></i>
                    </button>
                    <div class="search_clear">取消</div>
                </form>
            </div>
        </div>
        <div class="search_results">
            <ul class="drop-list" style="display: none;">
                <li>案例</li>
                <li>案例实例</li>
                <li>案例价格</li>
            </ul>
            <section class="result_block history_word">
                <h4>搜索历史</h4>
                <div class="result_words">
                    <a href="#">全部模板</a>
                    <a href="#">艺术设计</a>
                    <a href="#">摄影摄像</a>
                    <a href="#">建筑园林</a>
                </div>
                <div class="clear_btn">
                    <i class="iconfont icon-lajitong"></i>
                </div>
            </section>
            <section class="result_block hot_word">
                <h4>热门搜索</h4>
                <div class="result_words">
                    <a href="#">全部模板</a>
                    <a href="#">艺术设计</a>
                    <a href="#">摄影摄像</a>
                    <a href="#">建筑园林</a>
                    <a href="#">全部模板</a>
                    <a href="#">艺术设计</a>
                    <a href="#">摄影摄像</a>
                    <a href="#">建筑园林</a>
                </div>
            </section>
        </div>
    </div>
    <div id="side-nav" class="side-nav">
        <div class="nav_container">
            <div class="nav_wrapper">
                <a class="nav_item wow active" href="#">关于我们</a>
                <a class="nav_item wow" href="#">相关教程</a>
                <a class="nav_item wow" href="#">常见问题</a>
                <a class="nav_item wow" href="#">业务价格</a>
                <a class="nav_item wow" href="#">法律声明</a>
                <a class="nav_item wow" href="#">联系我们</a>
            </div>
            <div class="contact">
                <div class="tel wow">010-69557550</div>
                <div class="workday wow">周一至周五 9:00 - 18:00</div>
                <div class="address wow">
                    <div>北京市朝阳区</div>
                    <div>建外SOHO东区2号楼</div>
                </div>
                <div class="share wow">
                    <a class="weixin" href="javascript:;" data-src="../assets/images/uemo_code.jpg">
                        <i class="iconfont icon-weixin1"></i>
                    </a>
                    <a class="weibo" href="http://service.weibo.com/share/share.php?appkey=3206975293&">
                        <i class="iconfont icon-weibo1"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>