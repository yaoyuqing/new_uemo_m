<!-- 底部 -->
<footer id="footer">
  <div class="module_container">
    <a href="./index.html" class="footer-item {{if active === 'index'}} active {{/if}}">
      <i class="icon-default iconfont icon-shouye"></i>
      <i class="icon-active iconfont icon-shouye-active"></i>
      <span>首页</span>
    </a>
    <a href="./template.html" class="footer-item {{if active === 'template'}} active {{/if}}">
      <i class="icon-default iconfont icon-fenlei"></i>
      <i class="icon-active iconfont icon-fenlei-active"></i>
      <span>模板</span>
    </a>
    <a href="./customer.html" class="footer-item {{if active === 'customer'}} active {{/if}}">
      <i class="icon-default iconfont icon-anli"></i>
      <i class="icon-active iconfont icon-anli-active"></i>
      <span>案例</span>
    </a>
    <a href="./news.html" class="footer-item {{if active === 'news'}} active {{/if}}">
      <i class="icon-default iconfont icon-bangzhu1"></i>
      <i class="icon-active iconfont icon-bangzhu-active"></i>
      <span>帮助</span>
    </a>
    <a href="./user.html" class="footer-item {{if active === 'user'}} active {{/if}}">
      <i class="icon-default iconfont icon-touxiang"></i>
      <i class="icon-active iconfont icon-touxiang-active"></i>
      <span>我的</span>
    </a>
  </div>
</footer>
</body>

</html>