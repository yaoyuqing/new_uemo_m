{{include './partial/header.tpl' data={title:'uemo帮助中心',bodyClass:'bodynews'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div id="scrollView" class="scrollView">
    <div class="npagePage">
      <div class="news-serch">
        <div class="search_bar">
            <div class="search_bar_wrapper">
                <form action="" id="searchform" class="searchform">
                    <input type="text" placeholder="搜索" class="searchform_input" />
                    <button id="searchform_submit" class="searchform_submit" disabled="">
                        <i class="iconfont icon-sousuo-copy"></i>
                    </button>
                </form>
            </div>
        </div>
      </div>
      <div id="page_target" class="page_target">
        <div class="container_category">
            <div class="category_wrapper" data-menu-title="全部">
              <a href="#" class="active">全部</a>
              <a href="#">选择模板</a>
              <a href="#">SEO</a>
              <a href="#">域名问题</a>
              <a href="#">特色功能</a>
              <a href="#">管理网站</a>
              <a href="#">新闻资讯</a>
              <a href="#">使用教程</a>
            </div>
            <div class="current_category">
              <span class="text">全部</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </div>
          </div>
      </div>
      <div class="category_mask"></div>
      <div id="page_content" class="page_content">
        <div class="search_tags">
            <span>热门搜索：</span>
            <a href="#">域名解析</a>
            <a href="#">网站添加视频</a>
            <a href="#">网站如何优化</a>
            <a href="#">网站使用流程</a>
            <a href="#">如何选择模板</a>
            <a href="#">功能详解</a>
        </div>
        <div class="mlist news">
          <div class="module_container">
            <div class="container_content">
              <div class="content_list">
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">个人、企业网站建设常见问题 Q&A</div>
                            <div class="des">
                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">魔艺建站 | 互联网上有哪些情报分析的网站、微</div>
                            <div class="des">
                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">模板建站后应该如何运用CNZZ查看网站数据？</div>
                            <div class="des">
                                做视频类网站，普通的包含视频的网站，和普通网站一样备案，但是如果是纯视频的平台网站，就需要办理《试听许可证》了。
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">网站首页集权排名，有哪些可行性操作？</div>
                            <div class="des">
                                当网站里大部分有排名页面都是内页，首页很少的时候，需要怎么把权重几种首页？
                            </div>
                        </div>
                    </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mask"></div>
</div>
{{include './partial/footer.tpl' data={active:'news'} }}