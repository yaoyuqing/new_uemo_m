{{include './partial/header.tpl' data={title:'uemo搜索',bodyClass:'bodysearch'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div id="scrollView" class="scrollView">
    <div class="npagePage">
      <div class="list-serch search-box">
        <div class="search_bar">
            <div class="search_bar_wrapper">
                <form action="" id="searchform" class="searchform">
                    <input type="text" placeholder="搜索" class="searchform_input" />
                    <button id="searchform_submit" class="searchform_submit" disabled="">
                        <i class="iconfont icon-sousuo-copy"></i>
                    </button>
                </form>
            </div>
            <p>搜索到“模板”共有149个结果</p>
        </div>
        <div class="page_category">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide ">
                <a href="javascript:;" class="item_category">
                    <span class="text">全部</span>
                    <span class="num">
                        (<span>121</span>)
                    </span>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="./template.html" class="item_category">
                    <span class="text">模板</span>
                    <span class="num">
                        (<span>121</span>)
                    </span>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="javascript:;" class="item_category">
                    <span class="text">新闻</span>
                    <span class="num">
                        (<span>121</span>)
                    </span>
                </a>
              </div>
              <div class="swiper-slide active">
                <a href="javascript:;" class="item_category">
                    <span class="text">案例</span>
                    <span class="num">
                        (<span>121</span>)
                    </span>
                </a>
              </div>
            </div>
            <div class="arrow_right">
              <i class="iconfont icon-jiantou5-copy-copy-copy"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="page_result">
        <div class="container_content">
          <div class="result_module template wow">
            <div class="title">
              <span class="text">模板</span>
              <span class="num">
                  (<span>121</span>)
              </span>
            </div>
            <div class="content_list">
              <div class="item_block">
                  <a href="./template_post.html">
                      <div class="item_img">
                          <img
                              src="./assets/images/template1.jpg"
                              alt=""
                          />
                      </div>
                      <div class="item_wrapper">
                          <p class="title">蔬菜水果网站</p>
                          <p class="subtitle">
                              <span class="num">编号</span>
                              <span class="id">mo005_21507</span>
                          </p>
                      </div>
                  </a>
              </div>
              <div class="item_block">
                  <a href="./template_post.html">
                      <div class="item_img">
                          <img
                              src="./assets/images/template1.jpg"
                              alt=""
                          />
                      </div>
                      <div class="item_wrapper">
                          <p class="title">蔬菜水果网站</p>
                          <p class="subtitle">
                              <span class="num">编号</span>
                              <span class="id">mo005_21507</span>
                          </p>
                      </div>
                  </a>
              </div>
            </div>
          </div>
          <div class="result_module news wow">
            <div class="title">
              <span class="text">新闻</span>
              <span class="num">
                  (<span>121</span>)
              </span>
            </div>
            <div class="content_list">
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">个人、企业网站建设常见问题 Q&A</div>
                            <div class="des">
                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item_block">
                    <a href="./news_post.html" class="item_box">
                        <div class="item_info">
                            <div class="title ellipsis">魔艺建站 | 互联网上有哪些情报分析的网站、微</div>
                            <div class="des">
                                魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                            </div>
                        </div>
                    </a>
                </div>
            </div>
          </div>
          <div class="result_module user wow">
            <div class="title">
              <span class="text">案例</span>
              <span class="num">
                  (<span>121</span>)
              </span>
            </div>
            <div class="content_list">
                <div class="item_block wow">
                  <a href="./preview.html" class="item_box">
                    <div class="head">
                      <i class="iconfont icon-gengduo"></i>
                    </div>
                    <div class="item_img">
                      <img src="./assets/images/template1.jpg" alt="" />
                    </div>
                    <div class="item_wrapper">
                      <div class="item_info">
                        <p class="title">耐飞影视</p>
                        <p class="subtitle">
                          北京网站建设案例，企业建站案例
                        </p>
                        <div class="tag">
                          <span>半定制</span>
                        </div>
                      </div>
                      <div class="drop_down">
                        <div class="btn">
                          <i class="iconfont icon-icon_more"></i>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="item_block wow">
                  <a href="./preview.html" class="item_box">
                    <div class="head">
                      <i class="iconfont icon-gengduo"></i>
                    </div>
                    <div class="item_img">
                      <img src="./assets/images/template1.jpg" alt="" />
                    </div>
                    <div class="item_wrapper">
                      <div class="item_info">
                        <p class="title">耐飞影视</p>
                        <p class="subtitle">
                          北京网站建设案例，企业建站案例
                        </p>
                        <div class="tag">
                          <span>半定制</span>
                        </div>
                      </div>
                      <div class="drop_down">
                        <div class="btn">
                          <i class="iconfont icon-icon_more"></i>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mask"></div>
</div>
{{include './partial/footer.tpl' data={active:'customer'} }}