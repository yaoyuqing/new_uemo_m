{{include './partial/header.tpl' data={title:'uemo案例',bodyClass:'bodytemplate'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
    <div id="scrollView" class="scrollView">
        <div class="npagePage">
            <div id="page_target" class="page_target">
                <div class="container_category">
                    <div class="current_category">
                        <span class="text">全部模板</span>
                        <i
                            class="iconfont icon-jiantou5-copy-copy-copy-copy"
                        ></i>
                    </div>
                    <div class="category_wrapper" data-menu-title="全部模板">
                        <a href="#" data-num="05">艺术设计</a>
                        <a href="#" data-num="05">摄影摄像</a>
                        <a href="#" class="active" data-num="05">建筑园林</a>
                        <a href="#" data-num="05">企业集团</a>
                        <a href="#" data-num="05">家居百货</a>
                        <a href="#" data-num="05">医疗保健</a>
                        <a href="#" data-num="05">餐饮茶酒</a>
                        <a href="#" data-num="05">农工林牧</a>
                        <a href="#" data-num="05">教育培训</a>
                    </div>
                </div>
            </div>
            <div id="page_content" class="page_content">
                <div class="perch-box"></div>
                <div class="mlist template">
                    <div class="content_list">
                        <div id="0" class="item_block wow">
                            <a href="./template_post.html">
                                <div class="item_img">
                                    <img
                                        src="./assets/images/template1.jpg"
                                        alt=""
                                    />
                                </div>
                                <div class="item_wrapper">
                                    <p class="title">蔬菜水果网站</p>
                                    <p class="subtitle">
                                        <span class="num">编号</span>
                                        <span class="id">mo005_21507</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item_block wow">
                            <a href="./template_post.html">
                                <div class="item_img">
                                    <img
                                        src="./assets/images/template1.jpg"
                                        alt=""
                                    />
                                </div>
                                <div class="item_wrapper">
                                    <p class="title">蔬菜水果网站</p>
                                    <p class="subtitle">
                                        <span class="num">编号</span>
                                        <span class="id">mo005_21507</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item_block wow">
                            <a href="./template_post.html">
                                <div class="item_img">
                                    <img
                                        src="./assets/images/template1.jpg"
                                        alt=""
                                    />
                                </div>
                                <div class="item_wrapper">
                                    <p class="title">蔬菜水果网站</p>
                                    <p class="subtitle">
                                        <span class="num">编号</span>
                                        <span class="id">mo005_21507</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item_block wow">
                            <a href="./template_post.html">
                                <div class="item_img">
                                    <img
                                        src="./assets/images/template1.jpg"
                                        alt=""
                                    />
                                </div>
                                <div class="item_wrapper">
                                    <p class="title">蔬菜水果网站</p>
                                    <p class="subtitle">
                                        <span class="num">编号</span>
                                        <span class="id">mo005_21507</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item_block wow">
                            <a href="./template_post.html">
                                <div class="item_img">
                                    <img
                                        src="./assets/images/template1.jpg"
                                        alt=""
                                    />
                                </div>
                                <div class="item_wrapper">
                                    <p class="title">蔬菜水果网站</p>
                                    <p class="subtitle">
                                        <span class="num">编号</span>
                                        <span class="id">mo005_21507</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pager">
                <li class="disabled"><span>上一页</span></li>
                <li><a href="/template/?page=2">下一页</a></li>
            </ul>
        </div>
    </div>
    <div class="mask"></div>
</div>
{{include './partial/footer.tpl' data={active:'template'} }}
