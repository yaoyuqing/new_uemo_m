<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>

  <body class="bodytemplatepost">
    <header id="header" class="header">
      <a href="javascript:;" class="back_btn">
        <i class="iconfont icon-jiantou5-copy-copy"></i>
      </a>
      <div class="post_id">
        <span class="text">编号</span>
        <span class="id">mo005_12446</span>
      </div>
      <a class="search" href="javascript:;">
        <i class="iconfont icon-sousuo-copy"></i>
      </a>
    </header>
    <div id="search-box" class="search-box">
      <div class="search_bar">
        <div class="search_bar_wrapper">
          <form action="" id="searchform" class="searchform">
            <input type="text" placeholder="" class="searchform_input" />
            <button id="searchform_submit" class="searchform_submit" disabled="">
              <i class="iconfont icon-sousuo-copy"></i>
            </button>
            <div class="search_clear">取消</div>
          </form>
        </div>
      </div>
      <div class="search_results">
        <ul class="drop-list" style="display: none;">
          <li>案例</li>
          <li>案例实例</li>
          <li>案例价格</li>
        </ul>
        <section class="result_block history_word">
          <h4>搜索历史</h4>
          <div class="result_words">
            <a href="#">全部模板</a>
            <a href="#">艺术设计</a>
            <a href="#">摄影摄像</a>
            <a href="#">建筑园林</a>
          </div>
          <div class="clear_btn">
            <i class="iconfont icon-lajitong"></i>
          </div>
        </section>
        <section class="result_block hot_word">
          <h4>热门搜索</h4>
          <div class="result_words">
            <a href="#">全部模板</a>
            <a href="#">艺术设计</a>
            <a href="#">摄影摄像</a>
            <a href="#">建筑园林</a>
            <a href="#">全部模板</a>
            <a href="#">艺术设计</a>
            <a href="#">摄影摄像</a>
            <a href="#">建筑园林</a>
          </div>
        </section>
      </div>
    </div>
    <div id="sitecontent" class="sitecontent">
      <div id="scrollView" class="scrollView">
        <div class="npagePage">
          <div class="banner">
            <div class="image" style="background-image:url(<%= require("./assets/images/template1.jpg") %>);"></div>
            <div class="post_btn_wrapper">
              <a href="./demo.html" class="post_btn preview">
                点击预览
              </a>
              <a href="#" class="post_btn try">
                免费试用
              </a>
            </div>
          </div>
          <div class="templatepost_title module wow">
            <div class="module_container">
              <p class="title">视频制作网站</p>
            </div>
          </div>
          <div class="user module">
            <div class="module_container">
              <div class="container_header wow">
                <p class="title">谁在用这套模板</p>
                <p class="subtitle">看看他们的网站，这套模板这么多行业也都可以用哦！</p>
              </div>
              <div class="container_content wow">
                <div class="content_wrapper swiper-container">
                  <div class="content_list swiper-wrapper">
                    <div class="item_block swiper-slide">
                      <a href="#" class="item_box">
                        <div class="head">
                          <i class="iconfont icon-gengduo"></i>
                        </div>
                        <div class="item_img">
                          <img src="./assets/images/template1.jpg" alt="">
                        </div>
                        <div class="item_wrapper">
                          <p class="title">福建十一启健康管理有限公司</p>
                          <div class="tags clearfix">
                            <span>艺术设计</span>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="item_block swiper-slide">
                      <a href="#" class="item_box">
                        <div class="head">
                          <i class="iconfont icon-gengduo"></i>
                        </div>
                        <div class="item_img">
                          <img src="./assets/images/template1.jpg" alt="">
                        </div>
                        <div class="item_wrapper">
                          <p class="title">福建十一启健康管理有限公司</p>
                          <div class="tags clearfix">
                            <span>艺术设计</span>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="item_block swiper-slide">
                      <a href="#" class="item_box">
                        <div class="head">
                          <i class="iconfont icon-gengduo"></i>
                        </div>
                        <div class="item_img">
                          <img src="./assets/images/template1.jpg" alt="">
                        </div>
                        <div class="item_wrapper">
                          <p class="title">福建十一启健康管理有限公司</p>
                          <div class="tags clearfix">
                            <span>艺术设计</span>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="listContent module">
            <div class="module_containet">
              <div class="container_header wow">
                <p class="title">相关模板</p>
              </div>
              <div class="container_category swiper-container wow">
                <div class="swiper-wrapper">
                  <a class="swiper-slide" href="#">全部模板</a>
                  <a class="swiper-slide" href="#">艺术设计</a>
                  <a class="swiper-slide" href="#">摄影摄像</a>
                  <a class="swiper-slide" href="#">建筑园林</a>
                  <a class="swiper-slide" href="#">艺术设计</a>
                </div>
              </div>
              <div class="container_content">
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    <div class="content_wrapper swiper-slide">
                      <div class="content_list clearfix">
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="content_wrapper swiper-slide">
                      <div class="content_list clearfix">
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="content_wrapper swiper-slide">
                      <div class="content_list clearfix">
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="content_wrapper swiper-slide">
                      <div class="content_list clearfix">
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="content_wrapper swiper-slide">
                      <div class="content_list clearfix">
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                        <div class="item_block wow">
                          <a href="./template_post.html">
                            <div class="item_img">
                              <img src="./assets/images/template1.jpg" alt="" />
                            </div>
                            <div class="item_wrapper">
                              <p class="title">
                                蔬菜水果网站
                              </p>
                              <p class="subtitle">
                                <span class="num">编号</span>
                                <span class="id">mo005_21507</span>
                              </p>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="abstract">
            <div class="module_container">
              <div class="container_header wow">
                <p class="title">产品摘要</p>
              </div>
              <div class="container_content wow">
                <p>
                  UEmo真正从企业用户角度出发，开发出自定义导航及页面、首页模块可显示/隐藏、拖拽排序、视差动效等功能，并同步生成手机站，也可绑定微信公众账号。
                </p>
                <p>
                  <br>
                </p>
                <p>
                  UEmo产品研发阶段中，在零推广的情况下，已有数万家企业及个人成为UEmo的忠实用户，并得到了极高的用户满意度。截止到目前，已有超百个城市代理伙伴与UEmo魔艺携手共进。
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer id="footer" class="footer">
      <div class="post_btn_wrapper">
        <a href="./demo.html" class="post_btn preview">
          点击预览
        </a>
        <a href="#" class="post_btn try">
          免费试用
        </a>
      </div>
    </footer>
    <div class="user-setting">
    <div class="user-setting_wrapper">
        <h4 class="title">需完善信息</h4>
        <form class="el-form">
          <div class="user-setting-personal_message-row">
            <div class="el-form-item ">
              <div class="el-form-item__content">
                <div
                  data-state="2"
                  class="page_component custom_el_input custom_el_input_required"
                >
                  <i class="custom_el_input-require_icon">*</i>
                  <div class="el-input">
                    <input
                      type="text"
                      autocomplete="off"
                      name="company_name"
                      class="el-input__inner"
                      placeholder="公司名称"
                    />
                  </div>
                  <div
                    class="custom_el_input-placeholder custom_el_input-placeholder"
                  ><span class="text err_msg">请输入公司名称</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="user-setting-personal_message-row">
            <div class="el-form-item ">
              <div class="el-form-item__content">
                <div
                  data-state="2"
                  class="page_component custom_el_input custom_el_input_required"
                >
                  <i class="custom_el_input-require_icon">*</i>
                  <div class="el-input">
                    <input
                      type="text"
                      autocomplete="off"
                      name="contacts"
                      class="el-input__inner"
                      placeholder="联系人"
                    />
                  </div>
                  <div
                    class="custom_el_input-placeholder custom_el_input-placeholder"
                  ><span class="text err_msg">请输入联系人</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="user-setting-personal_message-row">
            <div class="el-form-item ">
              <div class="el-form-item__content">
                <div
                  data-state="2"
                  class="page_component custom_el_input "
                >
                  <div class="el-input">
                    <input
                      type="text"
                      autocomplete="off"
                      name="email"
                      class="el-input__inner"
                      placeholder="邮箱"
                    />
                  </div>
                  <div
                    class="custom_el_input-placeholder custom_el_input-placeholder"
                  ><span class="text err_msg">请输入正确的邮箱</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="user-setting-personal_message-row">
            <div class="el-form-item">
              <div class="el-form-item__content">
                <div data-state="2" class="page_component custom_el_input">
                  <div class="el-input">
                    <input
                      type="text"
                      autocomplete="off"
                      name="qq"
                      class="el-input__inner"
                      placeholder="qq"
                    />
                    </div>
                    <div
                      class="custom_el_input-placeholder custom_el_input-placeholder"
                    ><span class="text err_msg"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="user-setting-personal_message-row">
              <div class="button submit_btn state-disable">
                <span class="text">保存</span>
              </div>
            </div>
          </form>
        </div>
      </div>
  </body>

</html>