{{include './partial/header.tpl' data={title:'uemo案例',bodyClass:'bodydiy'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div id="scrollView" class="scrollView">
    <div class="npagePage">
      <div class="case_btn">
        <a href="/customer/">模板案例</a>
        <a href="/diy/" class="active">定制案例</a>
      </div>
      <div id="page_target" class="page_target">
        <div class="container_category">
          <div class="current_category">
            <span class="text">全部模板</span>
            <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
          </div>
          <div class="category_wrapper" data-menu-title="全部案例">
            <a href="#">全部案例</a>
            <a href="#">艺术设计</a>
            <a href="#">摄影摄像</a>
            <a href="#">建筑园林</a>
            <a href="#">企业集团</a>
            <a href="#">家居百货</a>
            <a href="#">医疗保健</a>
            <a href="#">餐饮茶酒</a>
            <a href="#">农工林牧</a>
            <a href="#">教育培训</a>
          </div>
        </div>
      </div>
      <div id="page_content" class="page_content">
        <div class="perch-box"></div>
        <div class="mlist user">
          <div class="module_container">
            <div class="container_header wow">
              <p class="title">这些摄影摄像机构都在用</p>
              <p class="subtitle">
                看看他们的网站，这套模板这么多行业也都可以用哦！
              </p>
            </div>
            <div class="container_content">
              <div class="content_list">
                <div class="item_block wow">
                  <a href="./preview.html" class="item_box">
                    <div class="head">
                      <i class="iconfont icon-gengduo"></i>
                    </div>
                    <div class="item_img">
                      <img src="./assets/images/template1.jpg" alt="" />
                    </div>
                    <div class="item_wrapper">
                      <div class="item_info">
                        <p class="title">耐飞影视</p>
                        <p class="subtitle">
                          北京网站建设案例，企业建站案例
                        </p>
                        <div class="tag">
                          <span>半定制</span>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="item_block wow">
                  <a href="./preview.html" class="item_box">
                    <div class="head">
                      <i class="iconfont icon-gengduo"></i>
                    </div>
                    <div class="item_img">
                      <img src="./assets/images/template1.jpg" alt="" />
                    </div>
                    <div class="item_wrapper">
                      <div class="item_info">
                        <p class="title">耐飞影视</p>
                        <p class="subtitle">
                          北京网站建设案例，企业建站案例
                        </p>
                        <div class="tag">
                          <span>半定制</span>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mask"></div>
</div>
{{include './partial/footer.tpl' data={active:'customer'} }}