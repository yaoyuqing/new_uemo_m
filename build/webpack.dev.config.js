// const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');
const webpack = require('webpack');
const apiMocker = require('webpack-api-mocker');
module.exports = {
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    hot: true,
    useLocalIp: true,
    host: '0.0.0.0',
    port: 8080,
    open: true,
    compress: true,
    publicPath: '/',
    contentBase: false,
    openPage: 'index.html', // 指定默认启动浏览器时打开的页面
    index: 'index.html', // 指定首页位置
    // hotOnly: true, // HMR 不自动刷新
    inline: true, // 页面刷新
    noInfo: true, // webpack-dashboard
    stats: 'errors-only',
    // notifyOnErrors: true, // 跨平台错误提示
    overlay: true, // 在浏览器中显示错误
    before: function (app, server) {
      server._watch(`src/**.tpl`);
      server._watch(`src/*/**.tpl`);
      if (process.env.MOCK) {
        apiMocker(app, path.resolve('mock/api.js'), {
          proxy: {
            '/*': 'http://api.leaderlegend.com'
          }
        });
      }
    },
    proxy: {
      "/shop": {
        target: "http://new.uemo.net/",
        changeOrigin: true, // target是域名的话，需要这个参数，
        secure: false, // 设置支持https协议的代理
      },
    }
  },
  plugins: [
    // PC端
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/index.html'), // 最后生成的文件名
    //   template: 'src/pc/index.html', // 模版html
    //   chunks: ['vender', 'app'], // 注入打包后的js文件
    //   chunksSortMode: 'dependency',
    //   inject: true,
    //   favicon: path.resolve(__dirname, '../src/pc/assets/images/yinji.ico')
    // }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
    // 移动端
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/mobile_index.html'),
    //   template: 'src/mobile/index.html',
    //   chunksSortMode: 'dependency',
    //   chunks: ['vender', 'mobile'],
    //   inject: true
    // })
  ]
};
