const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
// happypack
const HappyPack = require('happypack');
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length });
const createHappyPlugin = (id, loaders) => new HappyPack({
  id: id,
  loaders: loaders,
  threadPool: happyThreadPool,
  verbose: true
});

const src = path.resolve(__dirname, 'src');
const config = require('../config');

// 控制台信息
// const Dashboard = require('webpack-dashboard');
// const DashboardPlugin = require('webpack-dashboard/plugin');
// const dashboard = new Dashboard();

// 模板引擎art-template
// var resolveFilename = require('art-template/lib/compile/adapter/resolve-filename.js')

// var srcResolve = path.resolve('./src')
// var isAlias = /^@/

// 多页面打包
const getFilesName = (filesPath) => {
  let files = glob.sync(filesPath);
  let entries = [];
  let entry, basename, extname;

  for (let i = 0; i < files.length; i++) {
    entry = files[i];
    extname = path.extname(entry); // 扩展名 eg: .html
    basename = path.basename(entry, extname); // 文件名 eg: index
    entries.push(basename);
  }
  return entries;
};

const pages = getFilesName('src/**.tpl'); // html模版文件

const isDev = process.env.NODE_ENV === 'development';

const webpackConfig = {
  entry: config.entry,
  mode: process.env.NODE_ENV || 'development',
  resolve: {
    modules: ['node_modules'],
    alias: {
      '@': path.resolve('src')
    }
  },
  output: {
    filename: isDev ? 'js/[name].[hash].js' : 'js/[name].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: isDev ? '/' : './'
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      // chunks: 'async', // async表示只对异步代码进行分割
      minSize: 30000, // 当超过指定大小时做代码分割
      // maxSize: 200000,  // 当大于最大尺寸时对代码进行二次分割
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '_',
      name: true,
      cacheGroups: { // 缓存组：如果满足vendor的条件，就按vender打包，否则按default打包
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10, // 权重越大，打包优先级越高
          // filename: 'js/vender.js'  //将代码打包成名为vender.js的文件
          name: 'vender'
        },
        default: {
          minChunks: 2,
          priority: -20,
          name: 'common',
          // filename: 'js/common.js',
          reuseExistingChunk: true // 是否复用已经打包过的代码
        }
      }
    }
  },
  plugins: [
    // copy custom static assets
    // new CopyWebpackPlugin([
    //   {
    //     from: path.resolve(__dirname, '../src/assets/css'),
    //     to: path.resolve(__dirname, '../dist/css'),
    //     ignore: ['.*']
    //   },
    //   {
    //     from: path.resolve(__dirname, '../src/assets/html'),
    //     to: path.resolve(__dirname, '../dist/html'),
    //     ignore: ['.*']
    //   }
    // ]),
    new HardSourceWebpackPlugin(),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: isDev ? 'css/[name].[hash].css' : 'css/[name].css'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    createHappyPlugin('happy-js', [{
      loader: 'babel-loader',
      options: {
        babelrc: true,
        cacheDirectory: true
      }
    }]),
    createHappyPlugin('happy-style', [
      'css-loader',
      {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          config: {
            path: path.resolve(__dirname, '../postcss.config.js')
          }
        }
      },
      'sass-loader'
    ])
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use: [
          isDev ? 'style-loader' : {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: true,
              reloadAll: true,
              publicPath: '../'
            }
          },
          'happypack/loader?id=happy-style'
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        include: src,
        use: 'happypack/loader?id=happy-js'
      },
      {
        test: /\.js$/,
        use: {
          loader: 'eslint-loader',
          options: {
            formatter: require('eslint-friendly-formatter') // 默认的错误提示方式
          }
        },
        enforce: 'pre', // 编译前检查
        exclude: /node_modules/, // 不检测的文件
        include: path.resolve(__dirname, '../src') // 要检查的目录
      },
      {
        test: /\.(gif|png|ico|jpg|svg)\??.*$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1,
            esModule: false,
            name: isDev ? 'assets/images/[name].[hash].[ext]' : 'assets/images/[name].[ext]' // 关闭路径 &context=./src'
            // pulbicPath: "./"
          }
        }]
      },
      {
        test: /\.(woff|ttf|eot|otf)\??.*$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1,
            esModule: false,
            name: isDev ? 'assets/fonts/[name].[hash].[ext]' : 'assets/fonts/[name].[ext]' // 关闭路径 &context=./src'
            // pulbicPath: "./"
          }
        }]
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: ['img:src', 'img:data-src', 'audio:src', 'link:href'],
            // minimize: true,
            interpolate: true // 内联样式背景图 html: background-image: url('${require(`./assets/images/*.jpg`)}'); ejs: background-image:url(<%= require("./assets/images/banner1.jpg") %>);
          }
        }
      }, {
        test: /\.tpl$/,
        loader: 'art-template-loader',
        options: {
          // art-template options (if necessary)
          // @see https://github.com/aui/art-template
          // resolveFilename: function (_path, context) {
          //   isAlias.test(_path) && (_path = _path.replace(isAlias, srcResolve))
          //   return resolveFilename(_path, context)
          // }
        }
      }
    ]
  }
};

pages.forEach(function (fileName) {
  let setting = {
    filename: fileName + '.html', // 生成的html存放路径，相对于path
    template: 'src/' + fileName + '.tpl', // html模板路径
    inject: true // js插入的位置，true/'head'/'body'/false
  };

  // setting.favicon = './src/assets/img/favicon.ico'
  setting.chunks = ['vender', 'app'];
  setting.chunksSortMode = 'dependency'; // manual
  setting.inject = 'body';
  setting.hash = isDev;
  setting.minify = false;
  webpackConfig.plugins.push(new HtmlWebpackPlugin(setting));
});

module.exports = webpackConfig;
